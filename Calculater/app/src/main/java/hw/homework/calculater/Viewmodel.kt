package hw.homework.calculater

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class Viewmodel : ViewModel(){
    val dialog = MutableLiveData<Int>()
    val toast = MutableLiveData<Int>()
    val snackbar = MutableLiveData<Int>()
    val ans = MutableLiveData<String>()
    val mem_ans = MutableLiveData<Double>()
    val pro_string = MutableLiveData<String>()
    private lateinit var digital : String
    private var invis_digital:Double = 0.0
    private var mem_digital:Double = 0.0
    private lateinit var process_str : String
    private lateinit var pre_calaulation : String
    private var flag=false
    private var flag2=false
    private var dot_flag=false
    private var dot_count =0
    private var num_count =0

    fun calculate(num: Int){
        if(num_count==0){
            digital=""
            pre_calaulation=""
        }
        if(pre_calaulation == "="){
            C()
        }
        if(num_count+dot_count>16) {
            dialog.value = 1
        }
        else {
            if (!dot_flag) {
                digital = digital + "$num"
                ans.value = digital
                num_count++
            } else if (dot_flag) {
                /*var num2 = num / 10
                for (i in 1..dot_count - 1) {
                    num2 = num2 / 10
                }*/
                digital = digital+"$num"
                ans.value = digital
                dot_count++
            }
        }
    }
    fun calculation(str:String){
        var temp = digital
        if(flag == true&&digital!=""){
            if (pre_calaulation == "+") {
                invis_digital = invis_digital + digital.toDouble()
            } else if (pre_calaulation == "-") {
                invis_digital = invis_digital - digital.toDouble()
            } else if (pre_calaulation == "x") {
                invis_digital = invis_digital * digital.toDouble()
            } else if (pre_calaulation == "÷") {
                invis_digital = invis_digital / digital.toDouble()
            }
            if(dot_flag){
                digital=invis_digital.toString()
            }
            else if(!dot_flag){
                digital=invis_digital.toInt().toString()
            }
        }
        if(digital==""){
            digital = invis_digital.toString()
        }
        if(str=="+"){
            process_str= "$digital+"
            pre_calaulation="+"
            pro_string.value=process_str
        }
        else if(str=="-"){
            process_str= "$digital-"
            pre_calaulation="-"
            pro_string.value=process_str
        }
        else if(str=="x"){
            process_str= "$digital"+"x"
            pre_calaulation="x"
            pro_string.value=process_str
        }
        else if(str=="÷"){
            process_str= "$digital÷"
            pre_calaulation="÷"
            pro_string.value=process_str
        }
        else if(str=="="&& temp!="") {
            if(flag2){
                temp=""
            }
            if(dot_flag){
                process_str= process_str+temp+"="
            }
            else if(!dot_flag){
                process_str= process_str+temp+"="
            }
            pre_calaulation="="
            pro_string.value=process_str
            process_str =""
        }
        invis_digital = digital.toDouble()
        ans.value = invis_digital.toString()
        digital=""
        flag=true
        if(pre_calaulation=="="){
            digital=invis_digital.toString()
            flag=false
        }
    }
    fun sign(){
        if(digital==""&&invis_digital==0.toDouble()){
            digital="0"
        }
        else if(invis_digital!=0.toDouble()&&digital==""){
            digital=invis_digital.toString()
        }
        digital="-"+digital
        ans.value=digital
    }
    fun dot(){
        if(!dot_flag) {
            dot_flag = true
        }
        if(dot_flag){
            digital=digital+"."
        }
        ans.value=digital
    }
    fun Square_root(){
        if(digital==""&&invis_digital==0.toDouble()){
            digital="0"
        }
        else if(invis_digital!=0.toDouble()&&digital==""){
            digital=invis_digital.toString()
        }
        process_str=process_str+"√($digital)"
        pro_string.value=process_str
        digital= (Math.sqrt(digital.toDouble())).toString()
        flag2=true
        calculation("=")
        flag2=false
    }
    fun square(){
        if(digital==""&&invis_digital==0.toDouble()){
            digital="0"
        }
        else if(invis_digital!=0.toDouble()&&digital==""){
            digital=invis_digital.toString()
        }
        process_str=process_str+"sqr($digital)"
        pro_string.value=process_str
        digital=(digital.toDouble()*digital.toDouble()).toString()
        flag2=true
        calculation("=")
        flag2=false
    }
    fun fraction(){
        if(digital==""&&invis_digital==0.toDouble()){
            digital="0"
        }
        else if(invis_digital!=0.toDouble()&&digital==""){
            digital=invis_digital.toString()
        }
        process_str=process_str+"1/$digital"
        pro_string.value=process_str
        digital = (1/digital.toDouble()).toString()
        dot_flag=true
        flag2=true
        calculation("=")
        flag2=false
    }
    fun delete(){
        if(digital!="") {
            if(digital.length>1) {
                digital = digital.substring(0 until digital.length - 1)
                if(dot_count!=0){
                    dot_count--
                }
                else if(num_count!=0) {
                    dot_flag=false
                    num_count--
                }
                ans.value = digital
            }
            else if(digital.length<=1){
                digital = ""
                ans.value = "0"
            }
        }
    }
    fun C(){
        invis_digital=0.toDouble()
        digital=""
        ans.value="0"
        process_str=""
        pro_string.value=""
        pre_calaulation=""
        flag=false
        dot_count=1
        dot_flag=false
        num_count=0
    }
    fun CE(){
        num_count=0
        digital=""
        ans.value="0"
    }
    fun remainder(){
        if(invis_digital==0.toDouble()&&digital==""){
            invis_digital=0.toDouble()
        }
        if(pre_calaulation=="+"||pre_calaulation=="-"){
            invis_digital=invis_digital*invis_digital/100
            ans.value = invis_digital.toString()
        }
        else if(pre_calaulation=="x"||pre_calaulation=="÷"){
            invis_digital=invis_digital/100
            ans.value = invis_digital.toString()
        }
        else{
            invis_digital=0.toDouble()
            ans.value = "0"
        }
    }
    fun memory(str: String){
        if(digital!=""){
            invis_digital=digital.toDouble()
        }
        else if(invis_digital==0.toDouble()&&digital!=""){
            invis_digital=digital.toDouble()
        }
        if(str=="MS"){ //無視目前記憶多少數字，直接以當前數字取代記憶中的數字
            mem_digital = invis_digital
            mem_ans.value=invis_digital
        }
        else if(str=="M_minus"){
            mem_digital -= invis_digital
            mem_ans.value=mem_digital
        }
        else if(str=="M_plus"){
            mem_digital += invis_digital
            mem_ans.value=mem_digital
        }
        else if(str=="MR"){ //Memory Recall的意思，將當前計算出來的數字呈現出來。
            invis_digital=mem_digital
            ans.value=invis_digital.toString()
        }
        else if(str=="MC" &&mem_digital!=0.toDouble()){
            mem_digital=0.toDouble()
            mem_ans.value=mem_digital
        }
        flag=false
    }
}