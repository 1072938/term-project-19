package hw.homework.calculater

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.activity_main.*
import androidx.lifecycle.Observer
import com.google.android.material.snackbar.Snackbar

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val viewModel = ViewModelProvider(this).get(Viewmodel::class.java)
        viewModel.ans.observe(this, Observer<String> { ans -> ans_text.text = ans.toString() })
        viewModel.mem_ans.observe(this, Observer<Double> { mem_ans -> memory_ans.text = mem_ans.toString() })
        viewModel.pro_string.observe(this, Observer<String> { pro_string -> process.text = pro_string.toString() })
        viewModel.dialog.observe(this, androidx.lifecycle.Observer { dialog1 ->if(dialog1==1)
                    AlertDialog.Builder(this)
                            .setTitle("錯誤")
                            .setMessage("輸入的值超過上限")
                            .setNegativeButton("取消",null)
                            .setPositiveButton("確認",null)
                            .show() })
        viewModel.snackbar.observe(this, androidx.lifecycle.Observer {
                    snackbar -> Snackbar.make(this.window.decorView.rootView, "計算結果無法修改!", Snackbar.LENGTH_SHORT).show() })
        viewModel.toast.observe(this, androidx.lifecycle.Observer
                { toast -> Toast.makeText(this,"無法執行該動作!", Toast.LENGTH_SHORT).show() })

        number_0.setOnClickListener{
            viewModel.calculate(0)
        }
        number_1.setOnClickListener{
            viewModel.calculate(1)
        }
        number_2.setOnClickListener{
            viewModel.calculate(2)
        }
        number_3.setOnClickListener{
            viewModel.calculate(3)
        }
        number_4.setOnClickListener{
            viewModel.calculate(4)
        }
        number_5.setOnClickListener{
            viewModel.calculate(5)
        }
        number_6.setOnClickListener{
            viewModel.calculate(6)
        }
        number_7.setOnClickListener{
            viewModel.calculate(7)
        }
        number_8.setOnClickListener{
            viewModel.calculate(8)
        }
        number_9.setOnClickListener{
            viewModel.calculate(9)
        }
        plus.setOnClickListener{
            viewModel.calculation("+")
        }
        minus.setOnClickListener{
            viewModel.calculation("-")
        }
        Multiply.setOnClickListener{
            viewModel.calculation("x")
        }
        devide.setOnClickListener{
            viewModel.calculation("÷")
        }
        equal.setOnClickListener{
            viewModel.calculation("=")
        }
        sign.setOnClickListener{ //正負號
            viewModel.sign()
        }
        dot.setOnClickListener{
            viewModel.dot()
        }
        Square_root.setOnClickListener{
            viewModel.Square_root()
        }
        square.setOnClickListener{
            viewModel.square()
        }
        fraction.setOnClickListener{
            viewModel.fraction()
        }
        delete.setOnClickListener{
            viewModel.delete()
        }
        C.setOnClickListener{
            viewModel.C()
        }
        CE.setOnClickListener{
            viewModel.CE()
        }
        remainder.setOnClickListener{
            viewModel.remainder()
        }
        MS.setOnClickListener{
            viewModel.memory("MS")
        }
        M_minus.setOnClickListener{
            viewModel.memory("M_minus")
        }
        M_plus.setOnClickListener{
            viewModel.memory("M_plus")
        }
        MR.setOnClickListener{
            viewModel.memory("MR")
        }
        MC.setOnClickListener{
            viewModel.memory("MC")
        }
    }
}